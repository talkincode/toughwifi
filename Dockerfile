FROM index.alauda.cn/toughstruct/tough-pypy
MAINTAINER jamiesun <jamiesun.net@gmail.com>

VOLUME ["/var/toughwifi"]

ADD etc/toughwifi.conf /etc/toughwifi.conf
ADD etc/supervisord.conf /etc/supervisord.conf
ADD bin/toughwifi /usr/bin/toughwifi

RUN chmod +x /usr/bin/toughwifi


RUN git clone -b master git@bitbucket.org:talkincode/toughwifi.git /opt/toughwifi
RUN ln -s /opt/toughwifi/toughctl /usr/bin/toughctl && chmod +x /usr/bin/toughctl
RUN ln -s /opt/toughwifi/etc/toughwifi /etc/toughwifi && chmod +x /usr/bin/toughwifi
RUN ln -s /opt/toughwifi/etc/supervisord.conf /etc/supervisord.conf
RUN ln -s /opt/toughwifi/etc/toughwifi.conf /etc/toughwifi.conf

EXPOSE 1815
EXPOSE 1812/udp
EXPOSE 1813/udp

VOLUME [ "/var/toughwifi" ]
ENTRYPOINT ["/usr/bin/toughwifi","start"]

