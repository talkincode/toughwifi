#!/usr/bin/env python
# coding=utf-8
import os
import cyclone.web
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from mako.lookup import TemplateLookup
from twisted.python import log
from twisted.internet import reactor
from toughwifi.common.permit import permit,load_handlers
from sqlalchemy.orm import scoped_session, sessionmaker
from toughwifi.common.dbengine import get_engine
from toughwifi.console import models
import time
import sys


###############################################################################
# web application
###############################################################################
class Application(cyclone.web.Application):
    def __init__(self, config=None,  **kwargs):
        self.config = config

        try:
            if 'TZ' not in os.environ:
                os.environ["TZ"] = config.defaults.tz
            time.tzset()
        except:
            pass


        self.db = scoped_session(sessionmaker(bind=get_engine(self.config), autocommit=False, autoflush=True))

        settings = dict(
            cookie_secret=os.environ.get('cookie_secret', "12oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo="),
            login_url="/login",
            template_path=os.path.join(os.path.dirname(__file__), "views"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
            db_engine = get_engine(self.config),
            api_secret=config.defaults.secret,
            debug=config.defaults.debug,
            db=self.db,
            xheaders=True,
            config=config
        )

        self.cache = CacheManager(**parse_cache_config_options({
            'cache.type': 'file',
            'cache.data_dir': '/tmp/cache/data',
            'cache.lock_dir': '/tmp/cache/lock'
        }))


        self.tp_lookup = TemplateLookup(directories=[settings['template_path']],
                                        default_filters=['decode.utf8'],
                                        input_encoding='utf-8',
                                        output_encoding='utf-8',
                                        encoding_errors='replace',
                                        module_directory="/tmp/toughwifi")

        self.init_route()
        cyclone.web.Application.__init__(self, permit.all_handlers, **settings)

    def init_route(self):
        handler_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "handlers")
        load_handlers(handler_path=handler_path, pkg_prefix="toughwifi.console.handlers")

        conn = self.db()
        oprs = conn.query(models.TlOperator)
        for opr in oprs:
            if opr.operator_type > 0:
                for rule in self.db.query(models.TlOperatorRule).filter_by(operator_name=opr.operator_name):
                    permit.bind_opr(rule.operator_name, rule.rule_path)
            elif opr.operator_type == 0:  # 超级管理员授权所有
                permit.bind_super(opr.operator_name)


def run(config):
    time.sleep(1.0)
    log.startLogging(sys.stdout)
    app = Application(config)
    reactor.listenTCP(int(config.server.port), app, interface=config.server.host)
    reactor.run()