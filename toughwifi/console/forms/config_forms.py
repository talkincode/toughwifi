#coding:utf-8

from toughwifi.common import pyforms
from toughwifi.common.pyforms import rules
from toughwifi.common.pyforms.rules import button_style, input_style

boolean = {0: u"否", 1: u"是"}
booleans = {'0': u"否", '1': u"是"}
timezones = {'CST-8': u"Asia/Shanghai"}

default_form = pyforms.Form(
    pyforms.Dropdown("debug", args=booleans.items(), description=u"开启DEBUG", help=u"开启此项，可以获取更多的系统日志纪录",**input_style),
    pyforms.Dropdown("tz", args=timezones.items(), description=u"时区", **input_style),
    pyforms.Textbox("secret", description=u"安全密钥", readonly="readonly", **input_style),
    # pyforms.Dropdown("ssl", args=booleans.items(), description=u"开启SSL", help=u"开启此项，可以使用安全HTTP访问", **input_style),
    # pyforms.Textbox("privatekey", description=u"安全证书路径", **input_style),
    # pyforms.Textbox("certificate", description=u"安全证书签名路径", **input_style),
    pyforms.Dropdown("syslog_enable", args=booleans.items(), description=u"开启syslog", help=u"开启此项，可以使用syslog服务",**input_style),
    pyforms.Textbox("syslog_server", description=u"syslog服务器", **input_style),
    pyforms.Textbox("syslog_port", description=u"syslog服务端口(UDP)", **input_style),
    pyforms.Button("submit", type="submit", html=u"<b>更新</b>", **button_style),
    title=u"系统配置管理",
    action="/config/default/update"
)

dbtypes = {'mysql': u"mysql", 'sqlite': u"sqlite"}

database_form = pyforms.Form(
    pyforms.Dropdown("echo", args=booleans.items(), description=u"开启数据库DEBUG", help=u"开启此项，可以在控制台打印SQL语句",**input_style),
    pyforms.Dropdown("dbtype", args=dbtypes.items(), description=u"数据库类型", **input_style),
    pyforms.Textbox("dburl", description=u"数据库连接字符串", **input_style),
    pyforms.Textbox("pool_size", description=u"连接池大小", **input_style),
    pyforms.Textbox("pool_recycle", description=u"连接池回收间隔（秒）", **input_style),
    pyforms.Textbox("backup_path", description=u"数据库备份路径", **input_style),
    pyforms.Button("submit", type="submit", html=u"<b>更新</b>", **button_style),
    title=u"数据库配置管理",
    action="/config/database/update"
)

influxdb_form = pyforms.Form(
    pyforms.Textbox("host", description=u"influxdb主机地址", **input_style),
    pyforms.Textbox("admin_port", description=u"管理端口", **input_style),
    pyforms.Textbox("udp_port", description=u"api端口(udp)", **input_style),
    pyforms.Textbox("dbname", description=u"默认数据库", **input_style),
    pyforms.Textbox("user", description=u"连接用户", **input_style),
    pyforms.Password("passwd", description=u"用户密码", **input_style),
    pyforms.Button("submit", type="submit", html=u"<b>更新</b>", **button_style),
    title=u"influxdb配置管理",
    action="/config/database/update"
)

nagios_form = pyforms.Form(
    pyforms.Textbox("nagios_bin", rules.len_of(1, 128), description=u"Nagios执行文件", readonly="readonly", **input_style),
    pyforms.Textbox("nagios_service", rules.len_of(1, 128), description=u"Nagios服务名", readonly="readonly", **input_style),
    pyforms.Textbox("nagios_cfg", rules.len_of(1, 128), description=u"Nagios配置文件", readonly="readonly", **input_style),
    pyforms.Textbox("nagios_host_group_cfg", rules.len_of(1, 128), description=u"Nagios主机组配置文件", readonly="readonly", **input_style),
    pyforms.Textbox("nagios_contact_cfg", rules.len_of(1, 128), description=u"Nagios联系人配置文件", readonly="readonly", **input_style),
    pyforms.Textbox("nagios_host_cfg_dir", rules.len_of(1, 128), description=u"Nagios主机配置目录", readonly="readonly", **input_style),    
    pyforms.Button("submit", type="submit", html=u"<b>更新</b>", **button_style),
    title=u"nagios配置管理",
    action="/config/nagios/update"
)


